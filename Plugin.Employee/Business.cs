﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Plugin.Employee
{
   public class Business
    {
        public static DataTable GetAllGrade()
        {
            Agile.DataAccess.DataContext context = new Agile.DataAccess.DataContext();
            //方式1
            //DataTable grades = context.Builder<DataTable>().Select("ID,Class,Level").From("Grade").QuerySingle();
            //方式2
            DataTable grades = context.Script("Select ID,Class,Level From Grade").QuerySingle<DataTable>();
            return grades;
        }
    }
}
