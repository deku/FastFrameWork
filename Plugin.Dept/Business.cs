﻿using FFW.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Plugin.Dept
{
    //Agile.DataAccess 数据库访问类库具体使用方法请参考 http://www.cnblogs.com/MuNet/p/5740833.html
    public class Business
    {
        public static DataTable GetAllGrade()
        {
            Agile.DataAccess.DataContext context = new Agile.DataAccess.DataContext();
            DataTable grades = context.Script("Select ID,Class,Level From Grade").QuerySingle<DataTable>();
            return grades;
        }
        public static List<Student> GetAllStudent()
        {
            Agile.DataAccess.DataContext context = new Agile.DataAccess.DataContext();
            //方式1
            //return context.Select("Student").QueryMany<Student>();
            //方式2
            //return context.Select<Student>().QueryMany();
            //方式3
            return context.Script("Select ID,Name,Age From Student").QueryMany<Student>();
        }
    }
}
