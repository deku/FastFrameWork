﻿// FastFrameWork 快速开发框架
// 创建时间：2015-06-23 22:56:33 
// 创建作者：穆建情
// 联系方式：66767376@QQ.Com
// 技术博客:http://www.cnblogs.com/MuNet/category/856588.html
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using System.Xml;
using System.IO;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraEditors;
using System.Configuration;
using System.Threading;
using System.Runtime.InteropServices;

namespace FFW.Bin
{
    public partial class RibbonForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public RibbonForm()
        {
            InitializeComponent();    
        }
        private void RibbonForm_Load(object sender, EventArgs e)
        {
            this.LoadConfig();
            if (ribbon.Pages.Count > 0)
                ribbon.SelectedPage = ribbon.Pages[0];
        }

        private void LoadConfig()
        {
            try
            {
                Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                foreach (var item in config.AppSettings.Settings.AllKeys)
                {
                    RibbonPage page = new RibbonPage();
                    page.Text = config.AppSettings.Settings[item].Key;
                    page.Tag = config.AppSettings.Settings[item].Value;
                    ribbon.Pages.Add(page);
                }
            }
            catch
            {
                throw new Exception("配置程序异常，请检查配置文件.");
            }
        }

        #region Ribbon_事件
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void ribbon_Merge(object sender, DevExpress.XtraBars.Ribbon.RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            RibbonControl childRibbon = e.MergedChild;
            parentRRibbon.StatusBar.MergeStatusBar(childRibbon.StatusBar);
        }

        private void ribbon_UnMerge(object sender, RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            parentRRibbon.StatusBar.UnMergeStatusBar();
        }

        private void ribbon_SelectedPageChanging(object sender, RibbonPageChangingEventArgs e)
        {
            try
            {
                Type type = Type.GetType(e.Page.Tag.ToString());
                var form = this.MdiChildren.FirstOrDefault(s => s.GetType().Equals(type));
                if (form == null)
                {
                    if (this.MdiChildren.Length > 0)
                        FFW.Utilitys.WaitControlEx.Show();
                    XtraForm child = System.Activator.CreateInstance(type) as XtraForm;
                    child.TopLevel = false;
                    child.TopMost = false;
                    child.MdiParent = this;
                    child.ShowInTaskbar = false;
                    child.FormBorderStyle = FormBorderStyle.None;
                    child.WindowState = FormWindowState.Maximized;
                    child.Dock = DockStyle.Fill;
                    child.Text = "";
                    child.Show();
                    FFW.Utilitys.WaitControlEx.Close();
                }
                else
                {
                    form.Activate();
                    form.BringToFront();
                }
            }
            catch (Exception ex)
            {   
                e.Cancel = true;
                FFW.Utilitys.WaitControlEx.Close();
                XtraMessageBox.Show("载入窗体时发生错误,详情：\r\n" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ribbon_ShowCustomizationMenu(object sender, RibbonCustomizationMenuEventArgs e)
        {
            e.ShowCustomizationMenu = false;
        }

        #endregion      



    }
}