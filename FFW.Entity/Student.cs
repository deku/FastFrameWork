﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FFW.Entity
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
